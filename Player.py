"""
MASSE Océane
Class Player
"""

################# IMPORTS

from typing import *

################# CLASS PLAYER

colours = {"yellow": 1, "red": 2}


class Player:
    """
    Class définissant ce qu'est un joueur
    Le nom permet d'expliciter si il s'agit d'un joueur ou d'une IA
    Chaque joueur est représenté par une couleur pour que cela soit plus explicite
    """

    def __init__(self, name: str, colour: str):
        self.name = name
        assert colour in colours
        self.colour: int = colours[colour]
