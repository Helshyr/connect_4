"""
MASSE Océane
Class BoardTree
"""

################ IMPORTS

from __future__ import (
    annotations,
)  #  corrige pblm de typing qui ne peut pas se référer à la classe actuelle

from Player import *
from Board import *


import numpy as np
from typing import *
import copy
import time
import random

################# CLASS GAME

class BoardTree:
  """
  Cette classe définit l'arbre de jeu. Chaque noeud est un board et ses enfants le sont également (d'où le nom BoardTree).
  Chaque noeud peut avoir jusqu'à 7 enfants 
  Depth est la profondeur de l'arbre
  """

  def __init__(self, board: Board, depth: int, player1: Player, player2: Player):
    self.player1 = player1
    self.player2 = player2
    self.depth = depth

    if depth == 0:
      self.board = board
      self.children = []
      return

    # génération des enfants
    self.board = board
    self.children = []
    for move in board.generate_moves():
      if depth % 2 == 0: # player 1 joue à profondeur impaire et player2 à profondeur paire
        self.children.append(BoardTree(board.create_board_with_next_move(move, player2), depth - 1, player1, player2)) 
      else:
        self.children.append(BoardTree(board.create_board_with_next_move(move, player1), depth - 1, player1, player2))
      
  def is_leaf(self) -> bool:
    """
    Vérifie si on est en bas de l'arbre ( donc une feuille)

    :return: booléen vrai ou faux
    """
    return len(self.children) == 0

  def my_turn_to_play(self, player_who_plays: Player) -> bool:
    """
    Vérifie si le joueur joue le bon tour (càd à la bonne profondeur)
    Player1 joue les tours impairs
    Player2 joue les tours pairs

    :param player_who_plays: joueur qui souhaite joueur

    :return: si c'est son tour ( vrai ou faux)
    """
    if self.depth % 2 == 0:
      return player_who_plays == self.player2
    else:
      return player_who_plays == self.player1

  def minmax(self, player: Player) -> Tuple(float, np.array):
    """
    Stratégie minmax, dont le but consiste à minimiser les pertes ( le gain adverse) et maximiser son score
    Sa compléxité repose notammment sur la profondeur de l'arbre de recherche
    Le score est calculé pour les feuilles, on remonte le score en cherchant à maximiser le sien, 
    et diminuer celui de l'adversaire

    :param player: joueur qui joue avec la strat du minmax

    :return: tuple avec le score et le board modifié avec le prochain coup (selon la strat minmax)
    """
    if self.is_leaf():
      return (self.board.board_final_score(player, self.player1, self.player2), self.board)

    score_and_child_list = []
    for child in self.children:
      score_and_child_list.append((child.minmax(player)[0], child.board.grid)) 

    if self.my_turn_to_play(player):
      return max(score_and_child_list, key=lambda x: x[0]) #score maximal (et board associé)
    else:
      return min(score_and_child_list, key=lambda x: x[0]) #score minimal (et board associé)